FROM fellipevidal/node-env

WORKDIR /usr/src/app

RUN npm install -g @nestjs/cli
RUN yarn install

CMD [ "yarn", "start:dev" ]

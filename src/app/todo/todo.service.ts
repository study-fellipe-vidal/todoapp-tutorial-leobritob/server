import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { CreateTodoDto } from './dto/create-todo.dto'
import { UpdateTodoDto } from './dto/update-todo.dto'
import { TodoEntity } from './entity/todo.entity'

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(TodoEntity)
    private readonly todoRepository: Repository<TodoEntity>
  ) {}

  async findAll() {
    return await this.todoRepository.find()
  }

  async findOne(id: string) {
    try {
      return await this.todoRepository.findOneOrFail(id)
    } catch (error) {
      throw new NotFoundException(error.message)
    }
  }

  async create(data: CreateTodoDto) {
    const newTodo = await this.todoRepository.create(data)
    return await this.todoRepository.save(newTodo)
  }

  async update(id: string, data: UpdateTodoDto) {
    await this.todoRepository.findOneOrFail(id)
    await this.todoRepository.update(id, data)
    const todo = await this.todoRepository.findOneOrFail(id)

    return todo
  }

  async deleteById(id: string) {
    const todo = await this.todoRepository.findOneOrFail(id)
    return await this.todoRepository.softRemove(todo)
  }
}

import { IsIn, IsNotEmpty, IsString } from 'class-validator'

export class CreateTodoDto {
  @IsNotEmpty()
  @IsString()
  task: string

  @IsNotEmpty()
  @IsIn([0, 1])
  isDone: number
}
